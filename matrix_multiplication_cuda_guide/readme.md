# Matrix multiplication using shared memory

From the CUDA programming [guide](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory).

## Compile and run

```
nvcc matmul.cu -o matmul
./matmul 

nvcc matmul_shared.cu -o matmul_shared
./matmul_shared

```