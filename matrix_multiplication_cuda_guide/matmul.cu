#include <stdio.h>
#include <chrono>

// Matrices are stored in row-major order:
// M(row, col) = *(M.elements + row * M.width + col)
typedef struct { // typedef <existing_name> <alias_name>  
    int width;
    int height;
    float* elements;
} Matrix;


// Thread block size
#define BLOCK_SIZE 32
// Total maximum number of threads per block: 1024.

// Forward declaration of the matrix multiplication kernel
__global__ void MatMulKernel(const Matrix, const Matrix, Matrix);

// Matrix multiplication - Host code
// Matrix dimensions are assumed to be multiples of BLOCK_SIZE
void MatMul(const Matrix A, const Matrix B, Matrix C)
{
    // Load A and B to device memory
    Matrix d_A;
    d_A.width = A.width; d_A.height = A.height;
    size_t size = A.width * A.height * sizeof(float);
    cudaMalloc(&d_A.elements, size);
    cudaMemcpy(d_A.elements, A.elements, size,
               cudaMemcpyHostToDevice);
    Matrix d_B;
    d_B.width = B.width; d_B.height = B.height;
    size = B.width * B.height * sizeof(float);
    cudaMalloc(&d_B.elements, size);
    cudaMemcpy(d_B.elements, B.elements, size,
               cudaMemcpyHostToDevice);

    // Allocate C in device memory
    Matrix d_C;
    d_C.width = C.width; d_C.height = C.height;
    size = C.width * C.height * sizeof(float);
    cudaMalloc(&d_C.elements, size);

    // Invoke kernel
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid(B.width / dimBlock.x, A.height / dimBlock.y);

    // Timer
    auto t1 = std::chrono::high_resolution_clock::now();
    MatMulKernel<<<dimGrid, dimBlock>>>(d_A, d_B, d_C);
    cudaDeviceSynchronize();
    auto t2 = std::chrono::high_resolution_clock::now();
    auto t = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
    // Check that the kernel was executed successfully
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) {
        printf("Error: %s\n", cudaGetErrorString(err));
        return;
    }
    printf("Kernel exited\n");
    printf("Time: %f s\n", t.count() * 1e-6);

    // Read C from device memory
    cudaMemcpy(C.elements, d_C.elements, size,
               cudaMemcpyDeviceToHost);

    // Free device memory
    cudaFree(d_A.elements);
    cudaFree(d_B.elements);
    cudaFree(d_C.elements);
}

// Matrix multiplication kernel called by MatMul()
__global__ void MatMulKernel(Matrix A, Matrix B, Matrix C)
{
    // Each thread computes one element of C
    // by accumulating results into Cvalue
    float Cvalue = 0;
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    for (int e = 0; e < A.width; ++e)
        Cvalue += A.elements[row * A.width + e]
                * B.elements[e * B.width + col];
    C.elements[row * C.width + col] = Cvalue;
}

int main() {
    int N = 20000;
    int SIZE = N*N;

    // To avoid stack overflow, use new
    // The array is too big to fit in your program's stack region
    float* a = new float[SIZE];    // may throw std::bad_alloc
    float* b = new float[SIZE];
    float* c = new float[SIZE];

    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++){
            a[i*N + j] = (float)i*j;
            b[i*N + j] = (float)i*j*i*j;
        }
    }

    const Matrix A{N, N, a};
    const Matrix B{N, N, b};
    Matrix C{N, N, c};

    MatMul(A, B, C);

    // Checks
    // for (int i=0; i<N; i++){
    //     for (int j=0; j<N; j++){
    //         printf("A[%d,%d]: %f\n", i, j, A.elements[i*N + j]);
    //     }
    // }

    // for (int i=0; i<N; i++){
    //     for (int j=0; j<N; j++){
    //         printf("B[%d,%d]: %f\n", i, j, B.elements[i*N + j]);
    //     }
    // }

    // for (int i=0; i<N; i++){
    //     for (int j=0; j<N; j++){
    //         printf("C[%d,%d]: %f\n", i, j, C.elements[i*N + j]);
    //     }
    // }

    return 0;
}