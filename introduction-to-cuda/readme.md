# An Even Easier Introduction to CUDA

Link: https://developer.nvidia.com/blog/even-easier-introduction-cuda/.

## Compile and run

```
nvcc add.cpp -o add; ./add

nvcc add.cu -o add_cuda; ./add_cuda
nvprof ./add_cuda

nvcc add_block.cu -o add_block; ./add_block
nvprof ./add_block

nvcc add_grid.cu -o add_grid; ./add_grid
nvprof ./add_grid
```