# Matrix multiplication on GPU

Modified code from here: https://www.quantstart.com/articles/Matrix-Matrix-Multiplication-on-the-GPU-with-Nvidia-CUDA/

## Compile and run

```
nvcc matrixmul.cu -o matrixmul
./matrixmul
```