#include <iostream>
#include <vector>
#include <chrono>
#include "kernel.cu"
#include "dev_array.h"

int main()
{
    // Perform matrix multiplication C = A*B,
    // where A, B and C are NxN matrices.
    int N = 1000;
    int SIZE = N*N;

    // Allocate memory on the host.
    std::vector<float> h_A(SIZE);
    std::vector<float> h_B(SIZE);
    std::vector<float> h_C(SIZE);

    // Initialize matrices on the host.
    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++){
            h_A[i*N+j] = sin(i/N);
            h_B[i*N+j] = cos(j/N);
        }
    }

    // Allocate memory on the device.
    dev_array<float> d_A(SIZE);
    dev_array<float> d_B(SIZE);
    dev_array<float> d_C(SIZE);

    d_A.set(&h_A[0], SIZE);
    d_B.set(&h_B[0], SIZE);


    dim3 threadsPerBlock(N, N);
    dim3 blocksPerGrid(1, 1);
    if (N*N > 512) {
        // Max total number of threads in a block = 1024.
        threadsPerBlock.x = 512;
        threadsPerBlock.y = 512;
        blocksPerGrid.x = ceil(double(N)/double(threadsPerBlock.x));
        blocksPerGrid.y = ceil(double(N)/double(threadsPerBlock.y));
    }

    // Timer for GPU computation.
    auto t1_cuda = std::chrono::high_resolution_clock::now();
    matrixMultiplicationKernel<<<blocksPerGrid, threadsPerBlock>>>(d_A.getData(), d_B.getData(), d_C.getData(), N);
    cudaDeviceSynchronize();
    auto t2_cuda = std::chrono::high_resolution_clock::now();
    auto t_cuda = std::chrono::duration_cast<std::chrono::microseconds>(t2_cuda - t1_cuda);
    printf("GPU time: %f s\n", t_cuda.count() * 1e-6);

    d_C.get(&h_C[0], SIZE);
    cudaDeviceSynchronize();

    float *cpu_C;
    cpu_C=new float[SIZE];

    // Now do the matrix multiplication on the CPU
    // Timer for CPU computation.
    auto t1_cpu = std::chrono::high_resolution_clock::now();
    float sum;
    for (int row=0; row<N; row++){
        for (int col=0; col<N; col++){
            sum = 0.f;
            for (int n=0; n<N; n++){
                sum += h_A[row*N+n]*h_B[n*N+col];
            }
            cpu_C[row*N+col] = sum;
        }
    }
    auto t2_cpu = std::chrono::high_resolution_clock::now();
    auto t_cpu = std::chrono::duration_cast<std::chrono::microseconds>(t2_cpu - t1_cpu);
    printf("CPU time: %f s\n", t_cpu.count() * 1e-6);

    double err = 0;
    // Check the result and make sure it is correct
    for (int ROW=0; ROW < N; ROW++){
        for (int COL=0; COL < N; COL++){
            err += cpu_C[ROW * N + COL] - h_C[ROW * N + COL];
        }
    }

    std::cout << "Error: " << err << std::endl;

    return 0;
}